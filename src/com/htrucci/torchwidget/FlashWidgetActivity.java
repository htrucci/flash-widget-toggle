package com.htrucci.torchwidget;


import java.io.IOException;


import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.GetChars;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.RemoteViews;

@SuppressLint("NewApi")
public class FlashWidgetActivity extends AppWidgetProvider implements SurfaceHolder.Callback {

    private static final String ACTION_FLASH_ON ="com.htrucci.torchwidget.ON";
	private static final String ACTION_FLASH_OFF ="com.htrucci.torchwidget.OFF";
	private ComponentName flashWidget;
	private RemoteViews views = null;
	public static SurfaceView preview;
    public static SurfaceHolder mHolder;
	private static Camera camera;
	private static Camera.Parameters Parameters;
	int flashControl = 0;
	int abc=0 ,a=0;
	private Intent intent;
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onDeleted(context, appWidgetIds);
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// TODO Auto-generated method stub
		Log.e("Widget State","onUpdate");

		flashWidget = new ComponentName(context, FlashWidgetActivity.class);
		views = new RemoteViews(context.getPackageName(), R.layout.main);
		
		if(flashControl == 0){
	        if(camera == null ){
	        	Log.e("Widget State","CAMERA == NULL");
/*			camera = Camera.open();
	        Parameters = camera.getParameters();*/
	        }
			views.setImageViewResource(R.id.flash_btn, R.drawable.off);
			intent = new Intent(ACTION_FLASH_ON);
			
			
		}else if(flashControl == 1){
		
			views.setImageViewResource(R.id.flash_btn, R.drawable.on);
			intent = new Intent(ACTION_FLASH_OFF);
			
			
		}
		
		// Flash Intent
		PendingIntent onPendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		views.setOnClickPendingIntent(R.id.flash_btn, onPendingIntent);
		
		appWidgetManager.updateAppWidget(flashWidget, views);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		String action = intent.getAction();
		Log.e("Widget State","onReceive");

       
        Log.e("Widget State","onReceive3");
		
		
	    if(action.equals(ACTION_FLASH_ON)){
	    	
			Log.e("Flash state", intent.getAction());

				 Log.e("Widget State","CAMERA");
				 camera = Camera.open();
			        Parameters = camera.getParameters();
				 if(camera != null){
					Log.e("Widget State","CAMERA != NULL");
					Parameters.setFlashMode(Parameters.FLASH_MODE_TORCH); 
					camera.setParameters(Parameters);
					try {
						camera.setPreviewTexture(new SurfaceTexture(0));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					camera.startPreview();
				        Log.e("Widget State","onReceive3");
				        }
					


			try{
				flashControl = 1;
				
				AppWidgetManager manager = AppWidgetManager.getInstance(context);
			    this.onUpdate(context, manager, manager.getAppWidgetIds(new ComponentName(context, FlashWidgetActivity.class)));
			    
			}catch (Exception e) {
				// TODO: handle exception
				Log.e("Flash state", "Flash ON Exception");
			}
		}else if(action.equals(ACTION_FLASH_OFF)){
			
			Log.e("Flash state", intent.getAction());
			 if(camera != null){
					Log.e("Widget State","CAMERA != NULL");
					Parameters.setFlashMode(Parameters.FLASH_MODE_OFF); 
					Log.e("Widget State","CAMERA != NULL");
					/*camera.setParameters(Parameters);*/
					
/*					try {
						camera.setPreviewTexture(new SurfaceTexture(0));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					Log.e("Widget State","CAMERA != NULL");
					camera.release();
					camera = null;
				        Log.e("Widget State","onReceive3");
				        }

			try{

				flashControl = 0;
				
				AppWidgetManager manager = AppWidgetManager.getInstance(context);
			    this.onUpdate(context, manager, manager.getAppWidgetIds(new ComponentName(context, FlashWidgetActivity.class)));
			
			}catch (Exception e) {
				// TODO: handle exception
				Log.e("Flash state", "Flash OFF Exception");
			}
		}else{
			super.onReceive(context, intent);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
	
	}
}